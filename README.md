ego
---

ego-less tool for code review.

## Features

- about merge request:
    - [ ] I want to know if I have to review some merge requests.
    - [ ] I want to checkout the branch of a non-reviewed merge request.
    - [ ] I want to comment the merge request.
    - [ ] I want to approve a merge request.
    - [ ] I want to emoji reward a merge request.
    - [ ] I want to open my browser on the merge request URL for starting discussion.
- about todos:
    - [ ] I want to list my todos.
    - [ ] I want to select a todo and open my browser on the related URL of the todo.
    - [ ] I want to check a todo.

## Non-reviewed merge request

A non-reviewed merge request (abbr. NRMR):
- is not in Work In Progress state
- has not been rewarded with emoji by myself
- has not been approved by myself

## Milestones

- [ ] %1
- [ ] %2
- [ ] %3
- [ ] %4

## Contributing

```shell
export GOPATH=~/go
mkdir -p ${GOPATH}/src/gitlab.com/egolib/
cd ${GOPATH}/src/gitlab.com/egolib/
git clone git@gitlab.com:egolib/ego.git
cd ego
go get -v -t
go test -coverprofile cover.out
go tool cover -html=cover.out
go run ./egomain/main.go
go build -i -o ego egomain
```