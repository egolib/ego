package ego

import (
	"bytes"
	"errors"
	"fmt"
	. "github.com/logrusorgru/aurora"
	"github.com/stretchr/testify/assert"
	gtlb "github.com/xanzy/go-gitlab"
	"gopkg.in/kyokomi/emoji.v1"
	"testing"
)

func TestConsolePrinter_MergeRequests_WhenMergeRequests_WriteFancyPrint(t *testing.T) {
	asst := assert.New(t)
	expected := fmt.Sprintf(" - !%d ~ %s | %v %d %v %d %v 1\n   https://example.com/test \n\n",
		Cyan(100), Cyan("test"),
		emoji.Sprint(":thumbsup:"), Green(2),
		emoji.Sprint(":thumbsdown:"), Red(3),
		emoji.Sprint(":memo:"))

	buf := bytes.NewBufferString("")
	printer := NewPrinter(buf)

	mergeRequest := new(gtlb.MergeRequest)
	mergeRequest.IID = 100
	mergeRequest.Title = "test"
	mergeRequest.Upvotes = 2
	mergeRequest.Downvotes = 3
	mergeRequest.UserNotesCount = 1
	mergeRequest.WebURL = "https://example.com/test"

	var mrs = []*gtlb.MergeRequest{
		mergeRequest,
	}

	err := printer.MergeRequests(mrs)
	asst.Nil(err)
	asst.Equal(expected, buf.String())
}

type BrokenReadWriteSeeker struct {
}

func (brws BrokenReadWriteSeeker) Write(p []byte) (n int, err error) {
	return 0, errors.New("")
}

func (brws BrokenReadWriteSeeker) Read(p []byte) (n int, err error) {
	return 0, errors.New("")
}
func (brws BrokenReadWriteSeeker) Seek(offset int64, whence int) (int64, error) {
	return 0, errors.New("")
}

func TestConsolePrinter_WithBrokenWriter_MergeRequests_WhenMergeRequests_ReturnError(t *testing.T) {
	asst := assert.New(t)

	printer := NewPrinter(new(BrokenReadWriteSeeker))

	mergeRequest := new(gtlb.MergeRequest)
	mergeRequest.IID = 100
	mergeRequest.Title = "test"
	mergeRequest.Upvotes = 2
	mergeRequest.Downvotes = 3
	mergeRequest.UserNotesCount = 1
	mergeRequest.WebURL = "https://example.com/test"

	var mrs = []*gtlb.MergeRequest{
		mergeRequest,
	}

	err := printer.MergeRequests(mrs)
	asst.Error(err)
}

func TestConsolePrinter_Hosts_WhenHosts_WriteFancyPrint(t *testing.T) {
	asst := assert.New(t)
	expected := fmt.Sprintf(" - gitlab.com Gitlab (https://gitlab.com/api) \n")

	buf := bytes.NewBufferString("")
	printer := NewPrinter(buf)

	host := HostConfig{
		Provider:    "Gitlab",
		ApiEndpoint: "https://gitlab.com/api",
		Token:       "lol",
	}

	var hosts = make(map[string]HostConfig)
	hosts["gitlab.com"] = host

	err := printer.Hosts(hosts)
	asst.Nil(err)
	asst.Equal(expected, buf.String())
}

func TestConsolePrinter_WithBrokenWriter_Hosts_WhenHosts_ReturnError(t *testing.T) {
	asst := assert.New(t)

	printer := NewPrinter(new(BrokenReadWriteSeeker))

	host := HostConfig{
		Provider:    "Gitlab",
		ApiEndpoint: "https://gitlab.com/api",
		Token:       "lol",
	}

	var hosts = make(map[string]HostConfig)
	hosts["gitlab.com"] = host

	err := printer.Hosts(hosts)
	asst.Error(err)
}
