package ego

import (
	"github.com/stretchr/testify/assert"
	gtlb "github.com/xanzy/go-gitlab"
	"net/http"
	"net/http/httptest"
	"testing"
)

var diasporaGitlabProjectResponse = `
{
  "id": 3,
  "description": null,
  "default_branch": "master",
  "visibility": "private",
  "ssh_url_to_repo": "git@example.com:diaspora/diaspora-project-site.git",
  "http_url_to_repo": "http://example.com/diaspora/diaspora-project-site.git",
  "web_url": "http://example.com/diaspora/diaspora-project-site",
  "tag_list": [
    "example",
    "disapora project"
  ],
  "owner": {
    "id": 3,
    "name": "Diaspora",
    "created_at": "2013-09-30T13:46:02Z"
  },
  "name": "Diaspora Project Site",
  "name_with_namespace": "Diaspora / Diaspora Project Site",
  "path": "diaspora-project-site",
  "path_with_namespace": "diaspora/diaspora-project-site",
  "issues_enabled": true,
  "open_issues_count": 1,
  "merge_requests_enabled": true,
  "jobs_enabled": true,
  "wiki_enabled": true,
  "snippets_enabled": false,
  "container_registry_enabled": false,
  "created_at": "2013-09-30T13:46:02Z",
  "last_activity_at": "2013-09-30T13:46:02Z",
  "creator_id": 3,
  "namespace": {
    "id": 3,
    "name": "Diaspora",
    "path": "diaspora",
    "kind": "group",
    "full_path": "diaspora"
  },
  "import_status": "none",
  "import_error": null,
  "permissions": {
    "project_access": {
      "access_level": 10,
      "notification_level": 3
    },
    "group_access": {
      "access_level": 50,
      "notification_level": 3
    }
  },
  "archived": false,
  "avatar_url": "http://example.com/uploads/project/avatar/3/uploads/avatar.png",
  "shared_runners_enabled": true,
  "forks_count": 0,
  "star_count": 0,
  "runners_token": "b8bc4a7a29eb76ea83cf79e4908c2b",
  "public_jobs": true,
  "shared_with_groups": [
    {
      "group_id": 4,
      "group_name": "Twitter",
      "group_access_level": 30
    },
    {
      "group_id": 3,
      "group_name": "Gitlab Org",
      "group_access_level": 10
    }
  ],
  "repository_storage": "default",
  "only_allow_merge_if_pipeline_succeeds": false,
  "only_allow_merge_if_all_discussions_are_resolved": false,
  "request_access_enabled": false,
  "approvals_before_merge": 0,
  "statistics": {
    "commit_count": 37,
    "storage_size": 1038090,
    "repository_size": 1038090,
    "lfs_objects_size": 0,
    "job_artifacts_size": 0
  }
}`

var diasporaMergeRequestsReponse = `
[
  {
    "id": 1,
    "iid": 1,
    "target_branch": "master",
    "source_branch": "test1",
    "project_id": 3,
    "title": "test1",
    "state": "opened",
    "upvotes": 0,
    "downvotes": 0,
    "author": {
      "id": 1,
      "username": "admin",
      "email": "admin@example.com",
      "name": "Administrator",
      "state": "active",
      "created_at": "2012-04-29T08:46:00Z"
    },
    "assignee": {
      "id": 1,
      "username": "admin",
      "email": "admin@example.com",
      "name": "Administrator",
      "state": "active",
      "created_at": "2012-04-29T08:46:00Z"
    },
    "source_project_id": 2,
    "target_project_id": 3,
    "labels": [ ],
    "description": "fixed login page css paddings",
    "work_in_progress": false,
    "milestone": {
      "id": 5,
      "iid": 1,
      "project_id": 3,
      "title": "v2.0",
      "description": "Assumenda aut placeat expedita exercitationem labore sunt enim earum.",
      "state": "closed",
      "created_at": "2015-02-02T19:49:26.013Z",
      "updated_at": "2015-02-02T19:49:26.013Z",
      "due_date": null
    },
    "merge_when_pipeline_succeeds": true,
    "merge_status": "can_be_merged",
    "sha": "8888888888888888888888888888888888888888",
    "merge_commit_sha": null,
    "user_notes_count": 1,
    "should_remove_source_branch": true,
    "force_remove_source_branch": false,
    "web_url": "http://example.com/example/example/merge_requests/1"
  }
]`

func TestGitlab_GetProject_WhenDiasporaNamespace_ReturnDiasporaGitlabProject(t *testing.T) {
	asst := assert.New(t)

	var handler http.HandlerFunc = func(rw http.ResponseWriter, req *http.Request) {
		asst.Equal("/projects/diaspora%2Fdiaspora-project-site", req.URL.String())
		rw.Write([]byte(diasporaGitlabProjectResponse))
	}
	server := httptest.NewServer(handler)
	defer server.Close()

	gitlab := NewGitlabProvider(server.URL, "test")
	project, err := gitlab.GetProject("diaspora/diaspora-project-site")
	asst.Nil(err)
	asst.Equal(3, project.ID)
}

func TestGitlab_GetMergeRequests_WhenDiaporaGitlabProject_Return1MergeRequest(t *testing.T) {
	asst := assert.New(t)

	var handler http.HandlerFunc = func(rw http.ResponseWriter, req *http.Request) {
		asst.Equal("/projects/10/merge_requests?state=opened", req.URL.String())
		rw.Write([]byte(diasporaMergeRequestsReponse))
	}
	server := httptest.NewServer(handler)
	defer server.Close()

	gitlab := NewGitlabProvider(server.URL, "test")
	project := new(gtlb.Project)
	project.ID = 10

	mrs, err := gitlab.GetMergeRequests(project)
	asst.Nil(err)
	asst.Equal(1, len(mrs))
}

func TestGitlab_FilterNonReviewedMergeRequests_When2NRMRAnd2MR_Return2NRMR(t *testing.T) {
	asst := assert.New(t)

	var mergeRequests []*gtlb.MergeRequest

	mergeRequest1 := new(gtlb.MergeRequest)
	mergeRequest1.ID = 1
	mergeRequest1.WorkInProgress = false
	mergeRequests = append(mergeRequests, mergeRequest1)

	mergeRequest2 := new(gtlb.MergeRequest)
	mergeRequest2.ID = 2
	mergeRequest2.WorkInProgress = true
	mergeRequests = append(mergeRequests, mergeRequest2)

	mergeRequest3 := new(gtlb.MergeRequest)
	mergeRequest3.ID = 3
	mergeRequest3.WorkInProgress = true
	mergeRequests = append(mergeRequests, mergeRequest3)

	mergeRequest4 := new(gtlb.MergeRequest)
	mergeRequest4.ID = 4
	mergeRequest4.WorkInProgress = false
	mergeRequests = append(mergeRequests, mergeRequest4)

	gitlab := NewGitlabProvider("", "test")
	nonReviewedMergeRequests := gitlab.FilterNonReviewedMergeRequests(mergeRequests)

	asst.Equal(2, len(nonReviewedMergeRequests))
	asst.Equal(1, nonReviewedMergeRequests[0].ID)
	asst.Equal(4, nonReviewedMergeRequests[1].ID)
}
