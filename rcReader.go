package ego

import (
	"encoding/json"
	"errors"
	"io"
)

type RcReader interface {
	GetHostConfig(host string) (hostConfig HostConfig, err error)
	GetRC() (rc Rc)
}

type HostConfig struct {
	Token       string
	Provider    string
	ApiEndpoint string
}

type RcReaderImpl struct {
	rc Rc
}

var ErrHostNotFound = errors.New("Host not found.")

func (rcReader *RcReaderImpl) GetHostConfig(host string) (hostConfig HostConfig, err error) {
	value, ok := rcReader.rc.Hosts[host]

	if !ok {
		return hostConfig, ErrHostNotFound
	}

	return value, nil
}
func (rcReader *RcReaderImpl) GetRC() (rc Rc) {
	return rcReader.rc
}

func NewRCReader(reader io.Reader) (RcReader, error) {
	rcReader := new(RcReaderImpl)
	decoder := json.NewDecoder(reader)
	err := decoder.Decode(&rcReader.rc)
	if err != nil {
		return nil, err
	}
	return rcReader, nil
}
