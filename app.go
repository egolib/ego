package ego

import (
	"errors"
	"fmt"
	"github.com/spf13/afero"
	"io"
)

type App struct {
	fileSystem afero.Fs
	RC         afero.File
	output     io.Writer
}

func New(fs afero.Fs, f afero.File, w io.Writer) (app App) {
	app.fileSystem = fs
	app.RC = f
	app.output = w
	return app
}

var ErrProviderNotSupported = errors.New("Provider not supported.")

func (app App) MergeRequests() (err error) {

	repo, err := NewRepository(app.fileSystem)
	if err != nil {
		return err
	}
	gitConfigReader, err := repo.ReadGitConfig()
	if err != nil {
		return err
	}
	gitConfig, err := NewGitConfig(gitConfigReader)
	if err != nil {
		return err
	}

	remote, err := NewRemote(gitConfig.OriginUrl())
	if err != nil {
		return err
	}
	host := remote.GetHost()
	namespace := remote.GetNamespace()

	rcReader, err := NewRCReader(app.RC)
	if err != nil {
		return err
	}

	hostConfig, err := rcReader.GetHostConfig(host)
	if err != nil {
		return err
	}
	if hostConfig.Provider != "Gitlab" {
		return ErrProviderNotSupported
	}

	gtlb := NewGitlabProvider(fmt.Sprintf("%s/v3", hostConfig.ApiEndpoint), hostConfig.Token)
	project, err := gtlb.GetProject(namespace)
	if err != nil {
		return err
	}

	mergeRequests, err := gtlb.GetMergeRequests(project)
	if err != nil {
		return err
	}
	nonReviewedMergeRequests := gtlb.FilterNonReviewedMergeRequests(mergeRequests)

	frontend := NewPrinter(app.output)
	return frontend.MergeRequests(nonReviewedMergeRequests)
}

func (app App) HostList() (err error) {
	config, err := NewRCReader(app.RC)
	if err != nil {
		return err
	}
	rc := config.GetRC()
	frontend := NewPrinter(app.output)
	return frontend.Hosts(rc.Hosts)
}

func (app App) AddHost(host string, hostConfig HostConfig) (err error) {
	rcReader, err := NewRCReader(app.RC)
	if err != nil {
		return err
	}
	rc := rcReader.GetRC()
	rc.Hosts[host] = hostConfig
	rcWriter := NewRCWriter(app.RC)
	return rcWriter.Write(rc)
}

func (app App) RemoveHost(host string) (err error) {
	rcReader, err := NewRCReader(app.RC)
	if err != nil {
		return err
	}
	rc := rcReader.GetRC()
	delete(rc.Hosts, host)
	rcWriter := NewRCWriter(app.RC)
	return rcWriter.Write(rc)
}
