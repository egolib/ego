package ego

import (
	"encoding/json"
	"github.com/spf13/afero"
)

type RcWriter interface {
	Write(rc Rc) (err error)
}

type RcWriterImpl struct {
	file afero.File
}

type Rc struct {
	Hosts map[string]HostConfig
}

func (rcWriter *RcWriterImpl) Write(rc Rc) (error) {
	err := rcWriter.file.Truncate(0)
	if err != nil {
		return err
	}
	_, err = rcWriter.file.Seek(0,0)
	if err != nil {
		return err
	}
	encoder := json.NewEncoder(rcWriter.file)
	encoder.SetIndent("", "\t")
	err = encoder.Encode(rc)
	if err != nil {
		return err
	}
	return nil
}

func NewRCWriter(file afero.File) RcWriter {
	rcWriter := new(RcWriterImpl)
	rcWriter.file = file
	return rcWriter
}