FROM golang:1.8


WORKDIR /go/src/gitlab.com/egolib/ego
COPY . /go/src/gitlab.com/egolib/ego
RUN go get -v -t ./...
RUN go test -cover
RUN go install ./egomain
RUN mv /go/bin/egomain /go/bin/ego
ENTRYPOINT ["ego"]