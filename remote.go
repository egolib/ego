package ego

import (
	"errors"
	"strings"
)

type Remote interface {
	GetHost() (provider string)
	GetNamespace() (namespace string)
}

type RemoteImpl struct {
	host      string
	namespace string
}

func NewRemote(remoteString string) (Remote, error) {
	remote := new(RemoteImpl)

	indexEndProtocol := strings.Index(remoteString, "@")
	indexEndHost := strings.Index(remoteString, ":")

	if indexEndProtocol == -1 || indexEndHost == -1 {
		return nil, ErrRemoteFormatNotRecognized
	}

	remote.host = remoteString[indexEndProtocol+1 : indexEndHost]

	indexEndNamespace := strings.Index(remoteString, ".git")

	if indexEndNamespace == -1 {
		indexEndNamespace = len(remoteString)
	}

	remote.namespace = remoteString[indexEndHost+1 : indexEndNamespace]

	return remote, nil
}

var ErrRemoteFormatNotRecognized = errors.New("Remote format not recognized.")

func (remote RemoteImpl) GetHost() (provider string) {
	return remote.host
}

func (remote RemoteImpl) GetNamespace() (namespace string) {
	return remote.namespace
}
