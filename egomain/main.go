package main

import (
	"fmt"
	"github.com/mkideal/cli"
	"github.com/spf13/afero"
	"gitlab.com/egolib/ego"
	"log"
	"os"
	"os/user"
)

var help = cli.HelpCommand("display help information")

// root command
type rootT struct {
	Help bool `cli:"h,help" usage:"show help"`
}

var root = &cli.Command{
	Name: "home",
	Desc: "home command",
	Argv: func() interface{} { return new(rootT) },
	Fn: func(ctx *cli.Context) error {
		return nil
	},
}

type mergeRequestsT struct {
	cli.Helper
}

var mergeRequests = &cli.Command{
	Name:    "mergeRequests",
	Aliases: []string{"pullRequests", "mr", "pr"},
	Desc:    "get the merge requests of the current repository",
	Argv:    func() interface{} { return new(mergeRequestsT) },
	Fn: func(ctx *cli.Context) error {

		usr, err := user.Current()
		if err != nil {
			log.Fatal(err)
		}
		reader, err := os.Open(fmt.Sprintf("%s/.egorc", usr.HomeDir))
		if err != nil {
			log.Fatal(err)
		}

		app := ego.New(afero.NewOsFs(), reader, os.Stdout)
		err = app.MergeRequests()
		if err != nil {
			log.Fatal(err)
		}

		return nil
	},
}

type hostT struct {
	cli.Helper
}

var host = &cli.Command{
	Name:    "host",
	Aliases: []string{"h"},
	Desc:    "manipulate the host configuration",
	Argv:    func() interface{} { return new(hostT) },
	Fn: func(ctx *cli.Context) error {
		return nil
	},
}

type hostListT struct {
	cli.Helper
}

var hostList = &cli.Command{
	Name:    "list",
	Aliases: []string{"l"},
	Desc:    "list the hosts",
	Argv:    func() interface{} { return new(hostListT) },
	Fn: func(ctx *cli.Context) error {
		usr, err := user.Current()
		if err != nil {
			log.Fatal(err)
		}
		readWriter, err := os.Open(fmt.Sprintf("%s/.egorc", usr.HomeDir))
		if err != nil {
			log.Fatal(err)
		}

		app := ego.New(afero.NewOsFs(), readWriter, os.Stdout)
		err = app.HostList()
		if err != nil {
			log.Fatal(err)
		}

		return nil
	},
}

type addHostT struct {
	cli.Helper
	Hostname    string `cli:"*hostname" usage:"the hostname"`
	Token       string `cli:"*token" usage:"the token"`
	Provider    string `cli:"*provider" usage:"the provider"`
	ApiEndpoint string `cli:"*api" usage:"the api endpoint"`
}

var addHost = &cli.Command{
	Name:    "add",
	Aliases: []string{"a"},
	Desc:    "add a host",
	Argv:    func() interface{} { return new(addHostT) },
	Fn: func(ctx *cli.Context) error {
		argv := ctx.Argv().(*addHostT)
		usr, err := user.Current()
		if err != nil {
			log.Fatal(err)
		}
		readWriter, err := os.OpenFile(fmt.Sprintf("%s/.egorc", usr.HomeDir), os.O_RDWR, os.ModeExclusive)
		if err != nil {
			log.Fatal(err)
		}

		app := ego.New(afero.NewOsFs(), readWriter, os.Stdout)
		err = app.AddHost(argv.Hostname, ego.HostConfig{
			Token:       argv.Token,
			Provider:    argv.Provider,
			ApiEndpoint: argv.ApiEndpoint,
		})

		if err != nil {
			log.Fatal(err)
		}

		return nil
	},
}

type removeHostT struct {
	cli.Helper
	Hostname string `cli:"*hostname" usage:"the hostname"`
}

var removeHost = &cli.Command{
	Name:    "remove",
	Aliases: []string{"r"},
	Desc:    "remove a host",
	Argv:    func() interface{} { return new(removeHostT) },
	Fn: func(ctx *cli.Context) error {
		argv := ctx.Argv().(*removeHostT)
		usr, err := user.Current()
		if err != nil {
			log.Fatal(err)
		}
		readWriter, err := os.OpenFile(fmt.Sprintf("%s/.egorc", usr.HomeDir), os.O_RDWR, os.ModeExclusive)
		if err != nil {
			log.Fatal(err)
		}

		app := ego.New(afero.NewOsFs(), readWriter, os.Stdout)
		err = app.RemoveHost(argv.Hostname)

		if err != nil {
			log.Fatal(err)
		}

		return nil
	},
}

func main() {
	if err := cli.Root(root,
		cli.Tree(help),
		cli.Tree(mergeRequests),
		cli.Tree(host, cli.Tree(hostList), cli.Tree(addHost), cli.Tree(removeHost)),
	).Run(os.Args[1:]); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
