package ego

import (
	"bytes"
	"fmt"
	"github.com/spf13/afero"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
	"os"
)

func getTestingDotEgoRc(hostname string, url string, provider string) (afero.File, afero.Fs, error) {
	mockedFs := afero.NewMemMapFs()
	egorc, err := mockedFs.Create(".egorc")
	if err != nil {
		return nil, nil, err
	}
	_, err = egorc.Write([]byte(fmt.Sprintf(`
{
	"Hosts": {
		"%s": {
			"Token": "test",
			"ApiEndpoint": "%s",
			"Provider": "%s"
		}
  	}
  }
	`, hostname, url, provider)))
	if err != nil {
		return nil, nil, err
	}
	egorc.Close()
	mockFile, err := mockedFs.OpenFile(".egorc", os.O_WRONLY, os.ModeExclusive)
	if err != nil {
		return nil, nil, err
	}
	return mockFile, mockedFs, nil
}

func getBrokenTestingDotEgoRc() (afero.File, error) {
	mockedFs := afero.NewMemMapFs()
	egorc, _ := mockedFs.Create(".egorc")
	_, err := egorc.Write([]byte(`{"Hosts}`))
	if err != nil {
		return nil, err
	}
	egorc.Close()
	mockFileWriter, err := mockedFs.OpenFile(".egorc", os.O_WRONLY, os.ModeExclusive)
	if err != nil {
		return nil, err
	}
	return mockFileWriter, nil
}

func TestApp_MergeRequests(t *testing.T) {
	asst := assert.New(t)

	projectCalled := false

	var handler http.HandlerFunc = func(rw http.ResponseWriter, req *http.Request) {
		if !projectCalled {
			asst.Equal("/v3/projects/diaspora%2Fdiaspora-project-site", req.URL.String())
			rw.Write([]byte(diasporaGitlabProjectResponse))
			projectCalled = true
		} else {
			asst.Equal("/v3/projects/3/merge_requests?state=opened", req.URL.String())
			rw.Write([]byte(diasporaMergeRequestsReponse))
		}
	}
	server := httptest.NewServer(handler)
	defer server.Close()

	providerUrl, _ := url.Parse(server.URL)

	gitConfig := fmt.Sprintf(diaporaGitConfig, providerUrl.Hostname())
	mockedFs := getTestingRepositoryFileSystem(".git/config", gitConfig)
	dotEgorcWriter, _, err := getTestingDotEgoRc(providerUrl.Hostname(), server.URL, "Gitlab")
	asst.Nil(err)

	buf := bytes.NewBufferString("")
	app := New(mockedFs, dotEgorcWriter, buf)
	err = app.MergeRequests()
	asst.Nil(err)
}

func TestApp_WithBrokenEgoRc_MergeRequests_ReturnError(t *testing.T) {
	asst := assert.New(t)

	gitConfig := fmt.Sprintf(diaporaGitConfig, "test.com")
	mockedFs := getTestingRepositoryFileSystem(".git/config", gitConfig)
	dotEgorcWriter, err := getBrokenTestingDotEgoRc()
	asst.Nil(err)

	app := New(mockedFs, dotEgorcWriter, ioutil.Discard)
	err = app.MergeRequests()
	asst.Error(err)
}

func TestApp_WithBrokenFs_MergeRequests_ReturnError(t *testing.T) {
	asst := assert.New(t)

	var mockedFs afero.Fs = new(MockedBrokenStatFs)

	dotEgorcWriter, _, err := getTestingDotEgoRc("test.com", "http://test.com/api", "Gitlab")
	asst.Nil(err)

	app := New(mockedFs, dotEgorcWriter, ioutil.Discard)
	err = app.MergeRequests()
	asst.Error(err)
}

func TestApp_WithUnreadableConfig_MergeRequests_ReturnError(t *testing.T) {
	asst := assert.New(t)

	var mockedFs afero.Fs = new(MockedBrokenOpenFs)

	dotEgorcWriter, _, err := getTestingDotEgoRc("test.com", "https://test.com/api", "Gitlab")
	asst.Nil(err)

	buf := bytes.NewBufferString("")
	app := New(mockedFs, dotEgorcWriter, buf)
	err = app.MergeRequests()
	asst.Error(err)
}
func TestApp_WithBrokenIni_MergeRequests_ReturnError(t *testing.T) {
	asst := assert.New(t)

	gitConfig := `
[cor
	url = diaspora/diaspora-project-site.git
	fetch = +refs/heads/*:refs/remotes/origin/*
[branch "master"]
	remote = origin
	merge = refs/heads/master
`
	mockedFs := getTestingRepositoryFileSystem(".git/config", gitConfig)
	dotEgorcWriter, _, err := getTestingDotEgoRc("test.com", "https://test.com/api", "Gitlab")
	asst.Nil(err)

	buf := bytes.NewBufferString("")
	app := New(mockedFs, dotEgorcWriter, buf)
	err = app.MergeRequests()
	asst.Error(err)
}

func TestApp_WithBrokenRemote_MergeRequests_ReturnError(t *testing.T) {
	asst := assert.New(t)

	gitConfig := `
[core]
	repositoryformatversion = 0
	filemode = true
	bare = false
	logallrefupdates = true
	ignorecase = true
	precomposeunicode = true
[remote "origin"]
	url = diaspora/diaspora-project-site.git
	fetch = +refs/heads/*:refs/remotes/origin/*
[branch "master"]
	remote = origin
	merge = refs/heads/master
`
	mockedFs := getTestingRepositoryFileSystem(".git/config", gitConfig)
	dotEgorcWriter, _, err := getTestingDotEgoRc("test.com", "http://test.com/api", "Gitlab")
	asst.Nil(err)

	buf := bytes.NewBufferString("")
	app := New(mockedFs, dotEgorcWriter, buf)
	err = app.MergeRequests()
	asst.Error(err)
}

func TestApp_WithUnsupportedProvider_MergeRequests_ReturnProviderNotSupportedError(t *testing.T) {
	asst := assert.New(t)

	gitConfig := fmt.Sprintf(diaporaGitConfig, "test.com")
	mockedFs := getTestingRepositoryFileSystem(".git/config", gitConfig)
	dotEgorcWriter, _, err := getTestingDotEgoRc("test.com", "http://test.com/api", "Lol")
	asst.Nil(err)

	app := New(mockedFs, dotEgorcWriter, ioutil.Discard)
	err = app.MergeRequests()
	asst.Error(err)
	asst.Equal(ErrProviderNotSupported, err)
}

func TestApp_WithUnknownHost_MergeRequests_ReturnHostNotFoundError(t *testing.T) {
	asst := assert.New(t)

	gitConfig := fmt.Sprintf(diaporaGitConfig, "test2.com")
	mockedFs := getTestingRepositoryFileSystem(".git/config", gitConfig)
	dotEgorcWriter, _, err := getTestingDotEgoRc("test1.com", "http://test.com/api", "Gitlab")
	asst.Nil(err)

	app := New(mockedFs, dotEgorcWriter, ioutil.Discard)
	err = app.MergeRequests()
	asst.Error(err)
	asst.Equal(ErrHostNotFound, err)
}

func TestApp_MergeRequests_WhenProjectNotFound_ReturnError(t *testing.T) {
	asst := assert.New(t)

	var handler http.HandlerFunc = func(rw http.ResponseWriter, req *http.Request) {
		asst.Equal("/v3/projects/diaspora%2Fdiaspora-project-site", req.URL.String())
		http.Error(rw, "404 not found", 404)
	}
	server := httptest.NewServer(handler)
	defer server.Close()

	providerUrl, _ := url.Parse(server.URL)

	gitConfig := fmt.Sprintf(diaporaGitConfig, providerUrl.Hostname())
	mockedFs := getTestingRepositoryFileSystem(".git/config", gitConfig)
	dotEgorcWriter, _, err := getTestingDotEgoRc(providerUrl.Hostname(), server.URL, "Gitlab")
	asst.Nil(err)

	buf := bytes.NewBufferString("")
	app := New(mockedFs, dotEgorcWriter, buf)
	err = app.MergeRequests()
	asst.Error(err)
}

func TestApp_MergeRequests_WhenMergeRequests500_ReturnError(t *testing.T) {
	asst := assert.New(t)

	projectCalled := false

	var handler http.HandlerFunc = func(rw http.ResponseWriter, req *http.Request) {
		if !projectCalled {
			asst.Equal("/v3/projects/diaspora%2Fdiaspora-project-site", req.URL.String())
			rw.Write([]byte(diasporaGitlabProjectResponse))
			projectCalled = true
		} else {
			http.Error(rw, "500 internal server error", 500)
		}
	}
	server := httptest.NewServer(handler)
	defer server.Close()

	providerUrl, _ := url.Parse(server.URL)

	gitConfig := fmt.Sprintf(diaporaGitConfig, providerUrl.Hostname())
	mockedFs := getTestingRepositoryFileSystem(".git/config", gitConfig)
	dotEgorcWriter, _, err := getTestingDotEgoRc(providerUrl.Hostname(), server.URL, "Gitlab")
	asst.Nil(err)

	buf := bytes.NewBufferString("")
	app := New(mockedFs, dotEgorcWriter, buf)
	err = app.MergeRequests()
	asst.Error(err)
}

func TestApp_HostList(t *testing.T) {
	asst := assert.New(t)

	gitConfig := fmt.Sprintf(diaporaGitConfig, "gitlab.com")
	mockedFs := getTestingRepositoryFileSystem(".git/config", gitConfig)
	dotEgorcWriter, _, err := getTestingDotEgoRc("gitlab.com", "https://gitlab.com/api", "Gitlab")
	asst.Nil(err)

	buf := bytes.NewBufferString("")
	app := New(mockedFs, dotEgorcWriter, buf)
	err = app.HostList()
	asst.Nil(err)
}

func TestApp_WithBrokenEgoRc_HostList_ReturnError(t *testing.T) {
	asst := assert.New(t)

	gitConfig := fmt.Sprintf(diaporaGitConfig, "gitlab.com")
	mockedFs := getTestingRepositoryFileSystem(".git/config", gitConfig)
	dotEgorcWriter, err := getBrokenTestingDotEgoRc()
	asst.Nil(err)

	buf := bytes.NewBufferString("")
	app := New(mockedFs, dotEgorcWriter, buf)
	err = app.HostList()
	asst.Error(err)
}

func TestApp_AddHost_WhenGitlabNet_WriteGitlabNet(t *testing.T) {
	asst := assert.New(t)

	expected := `{
	"Hosts": {
		"gitlab.com": {
			"Token": "test",
			"Provider": "Gitlab",
			"ApiEndpoint": "https://gitlab.com/api"
		},
		"gitlab.net": {
			"Token": "test",
			"Provider": "Gitlab",
			"ApiEndpoint": "https://gitlab.net/api"
		}
	}
}
`

	gitConfig := fmt.Sprintf(diaporaGitConfig, "gitlab.com")
	mockedFs := getTestingRepositoryFileSystem(".git/config", gitConfig)
	dotEgorcWriter, egoRcFs, err := getTestingDotEgoRc("gitlab.com", "https://gitlab.com/api", "Gitlab")
	asst.Nil(err)

	buf := bytes.NewBufferString("")
	app := New(mockedFs, dotEgorcWriter, buf)

	host := "gitlab.net"
	hostConfig := HostConfig{
		Token:       "test",
		Provider:    "Gitlab",
		ApiEndpoint: "https://gitlab.net/api",
	}

	err = app.AddHost(host, hostConfig)
	asst.Nil(err)
	mockFileReader, err := egoRcFs.OpenFile(".egorc", os.O_RDONLY, os.ModeExclusive)
	rcBytes, err := ioutil.ReadAll(mockFileReader)
	asst.Equal(expected, string(rcBytes))
}

func TestApp_WithBrokenWriter_AddHost_WhenGitlabNet_ReturnError(t *testing.T) {
	asst := assert.New(t)

	gitConfig := fmt.Sprintf(diaporaGitConfig, "gitlab.com")
	mockedFs := getTestingRepositoryFileSystem(".git/config", gitConfig)
	dotEgorcWriter, _ := mockedFs.Create(".egorc")

	buf := bytes.NewBufferString("")
	app := New(mockedFs, dotEgorcWriter, buf)

	host := "gitlab.net"
	hostConfig := HostConfig{
		Token:       "test",
		Provider:    "Gitlab",
		ApiEndpoint: "https://gitlab.net/api",
	}

	err := app.AddHost(host, hostConfig)
	asst.Error(err)
}
func TestApp_RemoveHost_WhenGitlabNet_WriteGitlabNet(t *testing.T) {
	asst := assert.New(t)

	expected := `{
	"Hosts": {}
}
`
	gitConfig := fmt.Sprintf(diaporaGitConfig, "gitlab.com")
	mockedFs := getTestingRepositoryFileSystem(".git/config", gitConfig)
	dotEgorcWriter, egoRcFs, err := getTestingDotEgoRc("gitlab.com", "https://gitlab.com/api", "Gitlab")
	asst.Nil(err)

	buf := bytes.NewBufferString("")
	app := New(mockedFs, dotEgorcWriter, buf)

	err = app.RemoveHost("gitlab.com")
	asst.Nil(err)
	mockFileReader, err := egoRcFs.OpenFile(".egorc", os.O_RDONLY, os.ModeExclusive)
	asst.Nil(err)
	rcBytes, err := ioutil.ReadAll(mockFileReader)
	asst.Equal(expected, string(rcBytes))
}

func TestApp_WithBrokenWriter_RemoveHost_WhenGitlabNet_ReturnError(t *testing.T) {
	asst := assert.New(t)

	gitConfig := fmt.Sprintf(diaporaGitConfig, "gitlab.com")
	mockedFs := getTestingRepositoryFileSystem(".git/config", gitConfig)
	dotEgorcWriter, _ := mockedFs.Create(".egorc")

	buf := bytes.NewBufferString("")
	app := New(mockedFs, dotEgorcWriter, buf)

	err := app.RemoveHost("gitlab.com")
	asst.Error(err)
}
