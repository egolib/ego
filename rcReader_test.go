package ego

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
)

func TestNewRCReader_WhenBrokenJson_ReturnNilAndError(t *testing.T) {
	asst := assert.New(t)

	reader := strings.NewReader(`
    "Hosts": {
		"test": {
			"Token": "test",
			"Provider": "Gitlab",
			"ApiEndpoint": "http://test/api/"
		},
		"test2": {
			"Token": "test2",
			"Provider": "Gitlab",
			"ApiEndpoint": "http://test2/api/"
		}
  	}
  }
	`)
	config, err := NewRCReader(reader)

	asst.Error(err)
	asst.Nil(config)
}

func TestRCReaderImpl_WithOneProvider_GetHostConfig_WhenGitlabCom_ReturnHostConfig(t *testing.T) {
	asst := assert.New(t)
	expectedToken := "MyToken"
	expectedProvider := "MyProvider"
	expectedApiEndpoint := "http://test/api"

	host := "gitlab.com"
	reader := strings.NewReader(fmt.Sprintf(`
  {
    "Hosts": {
		"%s": {
			"Token": "%s",
			"Provider": "%s",
			"ApiEndpoint": "%s"
		}
	}
  }
	`, host, expectedToken, expectedProvider, expectedApiEndpoint))

	config, err := NewRCReader(reader)
	asst.Nil(err)

	hostConfig, err := config.GetHostConfig(host)

	asst.Nil(err)
	asst.Equal(expectedToken, hostConfig.Token)
	asst.Equal(expectedProvider, hostConfig.Provider)
	asst.Equal(expectedApiEndpoint, hostConfig.ApiEndpoint)
}

func TestRCReaderImpl_WithTwoProviders_GetHostConfig_WhenGitlabCom_ReturnGitlabComHostConfig_And_WhenGitlabNet_ReturnGitlabNetHostConfig(t *testing.T) {
	asst := assert.New(t)
	expectedToken1 := "token1"
	expectedToken2 := "token2"
	expectedProvider1 := "Gitlab1"
	expectedProvider2 := "Gitlab2"
	expectedApiEndpoint1 := "http://gitlab.com/test1"
	expectedApiEndpoint2 := "http://gitlab.net/test2"

	host1 := "gitlab.com"
	host2 := "gitlab.net"
	reader := strings.NewReader(fmt.Sprintf(`
  {
    "Hosts": {
		"%s": {
			"Token": "%s",
			"Provider": "%s",
			"ApiEndpoint": "%s"
		},
		"%s": {
			"Token": "%s",
			"Provider": "%s",
			"ApiEndpoint": "%s"
		}
  	}
  }
	`, host1, expectedToken1, expectedProvider1, expectedApiEndpoint1, host2, expectedToken2, expectedProvider2, expectedApiEndpoint2))

	config, err := NewRCReader(reader)
	asst.Nil(err)

	hostConfig1, err1 := config.GetHostConfig(host1)
	hostConfig2, err2 := config.GetHostConfig(host2)

	asst.Nil(err1)
	asst.Equal(expectedToken1, hostConfig1.Token)
	asst.Equal(expectedProvider1, hostConfig1.Provider)
	asst.Equal(expectedApiEndpoint1, hostConfig1.ApiEndpoint)
	asst.Nil(err2)
	asst.Equal(expectedToken2, hostConfig2.Token)
	asst.Equal(expectedProvider2, hostConfig2.Provider)
	asst.Equal(expectedApiEndpoint2, hostConfig2.ApiEndpoint)
}

func TestRCReaderImpl_WithNoProvider_GetHostConfig_WhenGitlabCom_ShouldReturnEmptyAndError(t *testing.T) {
	asst := assert.New(t)

	reader := strings.NewReader(`
  {
    "Hosts": {}
  }`)

	config, err := NewRCReader(reader)
	asst.Nil(err)

	_, err = config.GetHostConfig("gitlab.com")

	asst.Error(err)
	asst.Equal(ErrHostNotFound, err)
}

func TestRCReaderImpl_WithHostGitlabComAndGitlabNet_GetRC_ShouldReturnHostListWithGitlabComAndGitlabNet(t *testing.T) {
	asst := assert.New(t)

	reader := strings.NewReader(`
  {
    "Hosts": {
		"gitlab.com": {
			"Token": "token1",
			"Provider": "Gitlab1",
			"ApiEndpoint": "https://gitlab1.com/api"
		},
		"gitlab.net": {
			"Token": "token2",
			"Provider": "Gitlab2",
			"ApiEndpoint": "https://gitlab2.com/api"
		}
  	}
  }`)

	rcReader, err := NewRCReader(reader)
	asst.Nil(err)

	rc := rcReader.GetRC()

	configHost1, ok1 := rc.Hosts["gitlab.com"]
	asst.True(ok1)
	asst.Equal("token1", configHost1.Token)
	asst.Equal("Gitlab1", configHost1.Provider)
	asst.Equal("https://gitlab1.com/api", configHost1.ApiEndpoint)

	configHost2, ok2 := rc.Hosts["gitlab.net"]
	asst.True(ok2)
	asst.Equal("token2", configHost2.Token)
	asst.Equal("Gitlab2", configHost2.Provider)
	asst.Equal("https://gitlab2.com/api", configHost2.ApiEndpoint)
}
