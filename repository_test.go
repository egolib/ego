package ego

import (
	"errors"
	"github.com/spf13/afero"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"os"
	"testing"
)

func getTestingRepositoryFileSystem(gitConfigLocation string, gitConfig string) afero.Fs {
	var mockedFs afero.Fs = afero.NewMemMapFs()
	mockedFs.MkdirAll(".git", 0755)
	afero.WriteFile(mockedFs, gitConfigLocation, []byte(gitConfig), 0644)
	return mockedFs
}

func TestNewRepository_WhenBrokenFs_ReturnNilAndError(t *testing.T) {
	asst := assert.New(t)

	var mockedFs afero.Fs = new(MockedBrokenStatFs)
	rep, err := NewRepository(mockedFs)

	asst.Error(err)
	asst.Nil(rep)
}

func TestNewRepository_WhenNoGit_ReturnNilAndGitConfigNotFoundError(t *testing.T) {
	asst := assert.New(t)

	mockedFs := getTestingRepositoryFileSystem(".git/test", "")
	rep, err := NewRepository(mockedFs)

	asst.Error(err)
	asst.Equal(err, ErrGitConfigNotFound)
	asst.Nil(rep)
}

type MockedBrokenStatFs struct {
	afero.Fs
}

func (brokenFs MockedBrokenStatFs) Stat(name string) (os.FileInfo, error) {
	return nil, errors.New("broken fs")
}

type MockedBrokenOpenFs struct {
	afero.Fs
}

func (brokenFs MockedBrokenOpenFs) Stat(name string) (fi os.FileInfo, err error) {
	return fi, nil
}

func (brokenFs MockedBrokenOpenFs) Open(name string) (afero.File, error) {
	return nil, errors.New("broken fs")
}

func TestAferoRepository_WithMyConf_GetGitConfigReader_ReturnMyConf(t *testing.T) {
	asst := assert.New(t)
	expected := "myConf"

	mockedFs := getTestingRepositoryFileSystem(".git/config", expected)
	rep, _ := NewRepository(mockedFs)
	gitConfigReader, _ := rep.ReadGitConfig()
	gitConfig, _ := ioutil.ReadAll(gitConfigReader)

	asst.Equal(expected, string(gitConfig))
}
