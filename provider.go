package ego

import (
	gtlb "github.com/xanzy/go-gitlab"
)

type Provider interface {
	GetProject(namespace string) (project *gtlb.Project, err error)
	GetMergeRequests(project *gtlb.Project) (mergeRequests []*gtlb.MergeRequest, err error)
	FilterNonReviewedMergeRequests(mergeRequests []*gtlb.MergeRequest) (nonReviewedMergeRequests []*gtlb.MergeRequest)
}

type Gitlab struct {
	client *gtlb.Client
}

func (provider Gitlab) GetProject(namespace string) (project *gtlb.Project, err error) {
	project, _, err = provider.client.Projects.GetProject(namespace)
	return
}

func (provider Gitlab) GetMergeRequests(project *gtlb.Project) (mergeRequests []*gtlb.MergeRequest, err error) {
	options := new(gtlb.ListMergeRequestsOptions)
	state := "opened"
	options.State = &state
	mergeRequests, _, err = provider.client.MergeRequests.ListMergeRequests(project.ID, options)
	return
}

func (provider Gitlab) FilterNonReviewedMergeRequests(mergeRequests []*gtlb.MergeRequest) (nonReviewedMergeRequests []*gtlb.MergeRequest) {
	for i := range mergeRequests {
		if !mergeRequests[i].WorkInProgress {
			nonReviewedMergeRequests = append(nonReviewedMergeRequests, mergeRequests[i])
		}
	}

	return nonReviewedMergeRequests
}

func NewGitlabProvider(url string, token string) Provider {
	gitlab := new(Gitlab)
	gitlab.client = gtlb.NewClient(nil, token)
	gitlab.client.SetBaseURL(url)
	return gitlab
}
