package ego

import (
	"github.com/knq/ini"
	"io"
)

type GitConfig interface {
	OriginUrl() (url string)
}

type GitConfigImpl struct {
	originUrl string
}

func (git *GitConfigImpl) OriginUrl() (url string) {
	return git.originUrl
}

func NewGitConfig(reader io.Reader) (GitConfig, error) {
	gitConfig, err := ini.Load(reader)
	if err != nil {
		return nil, err
	}

	gitConfig.SectionManipFunc = ini.GitSectionManipFunc
	gitConfig.SectionNameFunc = ini.GitSectionNameFunc

	git := new(GitConfigImpl)
	git.originUrl = gitConfig.GetKey("remote.origin.url")

	return git, nil
}
