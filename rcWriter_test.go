package ego

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"github.com/spf13/afero"
	"io/ioutil"
	"os"
	"errors"
)

func TestRCWriterImpl_Write_WhenRC_WriteRC(t *testing.T) {
	asst := assert.New(t)

	expected := getRcWith2Hosts()

	mockfs := afero.NewMemMapFs()
	mockFile, err := mockfs.Create(".egorc")
	asst.Nil(err)
	mockFile.Write([]byte(getRcWith1Host()))
	mockFileWriter, err := mockfs.OpenFile(".egorc", os.O_WRONLY, os.ModeExclusive)
	asst.Nil(err)
	rcWriter := NewRCWriter(mockFileWriter)

	rc := Rc{Hosts: map[string]HostConfig{
		"test1": {Token: "test1", Provider: "Gitlab", ApiEndpoint: "http://test1/api/"},
		"test2": {Token: "test2", Provider: "Gitlab", ApiEndpoint: "http://test2/api/"},
	}}

	err = rcWriter.Write(rc)

	asst.Nil(err)
	mockFile.Seek(0, 0)
	content, err := ioutil.ReadAll(mockFile)
	asst.Nil(err)
	asst.Equal(expected, string(content))
}

func TestRCWriterImpl_WithFileBrokenTruncate_Write_WhenRC_ReturnError(t *testing.T) {
	asst := assert.New(t)

	rc := Rc{Hosts: map[string]HostConfig{
		"test1": {Token: "test1", Provider: "Gitlab", ApiEndpoint: "http://test1/api/"},
		"test2": {Token: "test2", Provider: "Gitlab", ApiEndpoint: "http://test2/api/"},
	}}

	rcWriter := NewRCWriter(new(FileWithBrokenTruncate))
	err := rcWriter.Write(rc)

	asst.Error(err)
}

func TestRCWriterImpl_WithFileBrokenSeek_Write_WhenRC_ReturnError(t *testing.T) {
	asst := assert.New(t)

	rc := Rc{Hosts: map[string]HostConfig{
		"test1": {Token: "test1", Provider: "Gitlab", ApiEndpoint: "http://test1/api/"},
		"test2": {Token: "test2", Provider: "Gitlab", ApiEndpoint: "http://test2/api/"},
	}}

	rcWriter := NewRCWriter(new(FileWithBrokenSeek))
	err := rcWriter.Write(rc)

	asst.Error(err)
}

func TestRCWriterImpl_Write_WhenEmpty_ReturnError(t *testing.T) {
	asst := assert.New(t)

	rc := Rc{Hosts: map[string]HostConfig{
		"test1": {Token: "test1", Provider: "Gitlab", ApiEndpoint: "http://test1/api/"},
		"test2": {Token: "test2", Provider: "Gitlab", ApiEndpoint: "http://test2/api/"},
	}}

	rcWriter := NewRCWriter(new(FileNonWritable))
	err := rcWriter.Write(rc)

	asst.Error(err)
}

func getRcWith1Host() string {
	return `{
	"Hosts": {
		"test1": {
			"Token": "test1",
			"Provider": "Gitlab",
			"ApiEndpoint": "http://test1/api/"
		}
	}
}
`
}

func getRcWith2Hosts() string {
	return `{
	"Hosts": {
		"test1": {
			"Token": "test1",
			"Provider": "Gitlab",
			"ApiEndpoint": "http://test1/api/"
		},
		"test2": {
			"Token": "test2",
			"Provider": "Gitlab",
			"ApiEndpoint": "http://test2/api/"
		}
	}
}
`
}

type FileWithBrokenTruncate struct {
	afero.File
}

func (fbt FileWithBrokenTruncate) Truncate(size int64) error {
	return errors.New("")
}

type FileWithBrokenSeek struct {
	afero.File
}

func (fbs FileWithBrokenSeek) Truncate(size int64) error {
	return nil
}

func (fbs FileWithBrokenSeek) Seek(offset int64, whence int) (int64, error) {
	return 0, errors.New("")
}
type FileNonWritable struct {
	afero.File
}

func (fu FileNonWritable) Truncate(size int64) error {
	return nil
}

func (fu FileNonWritable) Seek(offset int64, whence int) (int64, error) {
	return 0, nil
}
func (fu FileNonWritable) Write(p []byte) (n int, err error) {
	return 0, errors.New("")
}