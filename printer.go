package ego

import (
	"fmt"
	. "github.com/logrusorgru/aurora"
	gtlb "github.com/xanzy/go-gitlab"
	"gopkg.in/kyokomi/emoji.v1"
	"io"
)

type Printer interface {
	MergeRequests(mergeRequest []*gtlb.MergeRequest) (err error)
	Hosts(hosts map[string]HostConfig) (err error)
}

type ConsolePrinter struct {
	writer io.Writer
}

func (printer ConsolePrinter) MergeRequests(mergeRequests []*gtlb.MergeRequest) (err error) {
	for i := range mergeRequests {
		mergeRequest := mergeRequests[i]
		string := fmt.Sprintf(" - !%d ~ %s | %v %d %v %d %v %d\n   %s \n\n",
			Cyan(mergeRequest.IID), Cyan(mergeRequest.Title),
			emoji.Sprint(":thumbsup:"), Green(mergeRequest.Upvotes),
			emoji.Sprint(":thumbsdown:"), Red(mergeRequest.Downvotes),
			emoji.Sprint(":memo:"), mergeRequest.UserNotesCount,
			mergeRequest.WebURL)

		_, err := printer.writer.Write([]byte(string))
		if err != nil {
			return err
		}
	}
	return nil
}

func (printer ConsolePrinter) Hosts(hosts map[string]HostConfig) (err error) {
	for host := range hosts {
		hostConfig := hosts[host]
		string := fmt.Sprintf(" - %s %s (%s) \n",
			host, hostConfig.Provider, hostConfig.ApiEndpoint)

		_, err := printer.writer.Write([]byte(string))
		if err != nil {
			return err
		}
	}
	return nil
}

func NewPrinter(writer io.Writer) Printer {
	printer := new(ConsolePrinter)
	printer.writer = writer
	return printer
}
