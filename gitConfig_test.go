package ego

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
)

var diaporaGitConfig = `
[core]
	repositoryformatversion = 0
	filemode = true
	bare = false
	logallrefupdates = true
	ignorecase = true
	precomposeunicode = true
[remote "origin"]
	url = git@%s:diaspora/diaspora-project-site.git
	fetch = +refs/heads/*:refs/remotes/origin/*
[branch "master"]
	remote = origin
	merge = refs/heads/master
`

func TestNewGitConfig_WhenBrokenIni_ReturnNilAndError(t *testing.T) {
	asst := assert.New(t)

	reader := strings.NewReader(`
[core false
	logallrefupdates = true
	ignorecase = true
	precomposeunicode = true
[remo
	`)

	gitConfig, err := NewGitConfig(reader)
	asst.Error(err)
	asst.Nil(gitConfig)
}

func TestGitConfigImpl_WithDiasporaGitConfig_OriginUrl_ReturnDiasporaRemote(t *testing.T) {
	asst := assert.New(t)
	expected := "git@gitlab.com:diaspora/diaspora-project-site.git"

	reader := strings.NewReader(fmt.Sprintf(diaporaGitConfig, "gitlab.com"))

	git, err := NewGitConfig(reader)
	asst.Nil(err)
	config := git.OriginUrl()
	asst.Equal(expected, config)
}
