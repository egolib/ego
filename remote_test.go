package ego

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func GetRemoteTestWithRemoteString(remoteSring string, t *testing.T) (Remote, error, *assert.Assertions) {
	remote, err := NewRemote(remoteSring)
	return remote, err, assert.New(t)
}

func TestNewRemote_WhenUnrecognizedRemoteNoProtocol_ReturnNilAndRemoteFormatNotRecognizedError(t *testing.T) {
	remote, err, asst := GetRemoteTestWithRemoteString("gitlab.com:egolib/ego.git", t)

	asst.Error(err)
	asst.Equal(ErrRemoteFormatNotRecognized, err)
	asst.Nil(remote)
}

func TestNewRemote_WhenUnrecognizedRemoteNoHost_ReturnNilAndRemoteFormatNotRecognizedError(t *testing.T) {
	remote, err, asst := GetRemoteTestWithRemoteString("git@egolib/ego.git", t)

	asst.Error(err)
	asst.Equal(ErrRemoteFormatNotRecognized, err)
	asst.Nil(remote)
}

func TestRemoteImpl_GetProvider_WhenGitlabDotComRemote_ReturnGitlabDotCom(t *testing.T) {
	remote, err, asst := GetRemoteTestWithRemoteString("git@gitlab.com:egolib/ego.git", t)
	asst.Nil(err)

	provider := remote.GetHost()
	asst.Equal("gitlab.com", provider)
}

func TestRemoteImpl_GetNamespace_WhenEgoRemote_ReturnEgoNamespace(t *testing.T) {
	remote, err, asst := GetRemoteTestWithRemoteString("git@gitlab.com:egolib/ego.git", t)
	asst.Nil(err)

	namespace := remote.GetNamespace()
	asst.Equal("egolib/ego", namespace)
}

func TestRemoteImpl_GetNamespace_WhenEgoRemoteNoExtension_ReturnEgoNamespace(t *testing.T) {
	remote, err, asst := GetRemoteTestWithRemoteString("git@gitlab.com:egolib/ego", t)
	asst.Nil(err)

	namespace := remote.GetNamespace()
	asst.Equal("egolib/ego", namespace)
}
