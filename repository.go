package ego

import (
	"errors"
	"github.com/spf13/afero"
	"io"
)

type Repository interface {
	ReadGitConfig() (io.Reader, error)
}

type AferoRepository struct {
	fs afero.Fs
}

var ErrGitConfigNotFound = errors.New(".git/config not found")

func (rep AferoRepository) ReadGitConfig() (reader io.Reader, err error) {
	return rep.fs.Open(".git/config")
}

func NewRepository(fs afero.Fs) (Repository, error) {
	gitExist, err := afero.Exists(fs, ".git/config")
	if err != nil {
		return nil, err
	}
	if !gitExist {
		return nil, ErrGitConfigNotFound
	}

	rep := new(AferoRepository)
	rep.fs = fs
	return rep, nil
}
